﻿using System;
using FluentAssertions;
using FluentValidation;
using Kronos.Core.Features.Time;
using Xunit;

namespace Kronos.Core.Tests.Features.Time
{
  public class TimeEntryTests
  {
    [Fact]
    public void TimeEntryMustRequireAStartTime()
    {
#pragma warning disable CA1806 // Do not ignore method results
      Action act = () => { new TimeEntry(default, DateTimeOffset.MaxValue); };
#pragma warning restore CA1806 // Do not ignore method results

      act.Should().Throw<ValidationException>();
    }

    [Fact]
    public void TimeEntryShouldCalculateCorrectTimeSpan()
    {
      var start = DateTimeOffset.Now;
      var end = start.AddDays(2).AddHours(1);
      var te = new TimeEntry(start, end);
      te.CalculateTime().Should().Be(TimeSpan.FromDays(2).Add(TimeSpan.FromHours(1)));
    }

  }
}
