﻿using System;
using FluentValidation;

namespace Kronos.Core.Features.Time
{
  public class TimeEntry
  {
    public TimeEntry(DateTimeOffset start, DateTimeOffset end)
    {
      Start = start;
      End = end;

      var tev = new TimeEntryValidator();
      tev.ValidateAndThrow(this);
    }

    public DateTimeOffset Start { get; }
    public DateTimeOffset End { get; }

    public TimeSpan CalculateTime()
    {
      return End - Start;
    }
  }

  public class TimeEntryValidator : AbstractValidator<TimeEntry>
  {
    public TimeEntryValidator()
    {
      RuleFor(te => te.Start).NotEmpty().WithMessage("You must supply a start value.");
    }
  }
}